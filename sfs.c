/* Charles Gathuru
 * Student no: 260457189
 * Assignment 5
 * File: sfs.c
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <stdbool.h>
#include "sfs_api.h"
#include "disk_emu.h"

#define MAX_FILENAME_CHAR 32
#define ERROR -1
#define SUCCESS 0
#define BEGINNING 0
#define END -1
#define NOT_FOUND -1

#ifndef BLOCK_NUMBER
#define BLOCK_NUMBER 1000
#endif

#define FAT_BLOCKS BLOCK_NUMBER-3

#ifndef BLOCK_SIZE
#define BLOCK_SIZE 1024
#endif

#ifndef FILE_NUMBER
#define FILE_NUMBER 200
#endif

/**
 * Struce of a root directory table entry.
 */
typedef struct
{
	int fatIndex;
	char filename[MAX_FILENAME_CHAR];
	int size;
	time_t dateCreated;
	time_t dateModified;
	int readPtr;
	int writePtr;
	bool isEmpty;
}RDNode;

/**
 * Structure of the root directory
 */
typedef struct
{
	int index;
	RDNode table[FILE_NUMBER];
	int noFiles;
}RootDirectory;

/**
 * Strucutre of a file allocation table entry
 */
typedef struct fat_node
{
	int dbIndex; // The data block inex corresponding to the fat entry
	int next; // The next entry
	bool isEmpty; // If the node is empty
}FAT_NODE;

/**
 * Strucuture of the file allocation table
 */
typedef struct
{
	int index; // Index of the entry in the file allocation table
	FAT_NODE table[FAT_BLOCKS];
}FAT;

/**
 * Structure of the empty block table
 */
typedef struct
{
	bool table[BLOCK_NUMBER];
	int noEmpty;
}Empty;

/**
 * Structure of the open file table
 */
typedef struct
{
	int index;
	RDNode table[FILE_NUMBER];
	int noFiles;
}OFT;

RootDirectory rootDirectory; //the root directory
FAT fat; // the file allocation table
Empty empty; //the empty index
OFT oft; //the open file table

/* Function prototypes */
int getFileIndex(char *filename);
int getEmptyRootDirecetoryIndex();
int getEmptyOftIndex();
int getEmptyFatIndex();
int getEmptyBlockIndex();
time_t getTime();
bool isClosed(char *filename);
void readFromDisk();
void writeToDisk();



/**
 * Creates a new file system if fresh is indcated, otherwise
 * the current disk on the system is opened. Returns an error
 * if a new disk cannot be created or the disk could not be opened
 * @param  fresh Indicates if a new disk should be created
 * @return       Sucess or ERROR
 */
int mksfs(int fresh)
{
	if(fresh == 1)
	{
		/* Create a fresh disk with the given block size and block number */
		if(init_fresh_disk("disk", BLOCK_SIZE, BLOCK_NUMBER) == -1)
		{
			printf("Failed to create disk\n");
			exit(ERROR);
		}
		/* Start with all the blocks being empty */
		int i;
		for (i = 2; i < BLOCK_NUMBER-1; i++)
		{
			empty.table[i] = true;
			fat.table[i].isEmpty = true;
		}
		for (i = 0; i < FILE_NUMBER; i++)
		{
			oft.table[i].isEmpty = true;
			rootDirectory.table[i].isEmpty = true;
		}
		/* Set the number of empty blocks */
		empty.noEmpty = FAT_BLOCKS;

		/* Initilize all the indices of the tables to the starting index */
		oft.index = BEGINNING;
		fat.index = BEGINNING;
		rootDirectory.index = BEGINNING;

		printf("A disk has been created\n");
	}
	else
	{
		/* Open disk that is already on the system */
		if (init_disk("disk", BLOCK_SIZE, BLOCK_NUMBER) == ERROR)
		{
			perror("Failed to open the disk\n");
			exit(ERROR);
		}
		printf("Disk initialized sucessfully\n");

		int i;
		for (i = 0; i < FILE_NUMBER; i++)
		{
			oft.index = BEGINNING;
			/* Read the disk structure*/
			readFromDisk();
		}
	}
	return SUCCESS;
}

/**
 * Lists all the files in the file system in the format
 * Size: %d, Date created: %s, Date modified: %s
 */
void sfs_ls(void)
{
	int i;
	for (i = 0; i < FILE_NUMBER; i++)
	{
		if(!(oft.table[i].isEmpty))
		{
			int fileIndex = getFileIndex(oft.table[i].filename);
			if(fileIndex != NOT_FOUND)
				printf("%s\tSize:%d, Date created:%s, Date modified:%s\n",
					rootDirectory.table[i].filename, rootDirectory.table[i].size, ctime(&rootDirectory.table[i].dateCreated),
					ctime(&rootDirectory.table[i].dateModified));
		}
	}
}

/**
 * Opens a file with a given file name. If the file does not exist
 * a file with the given file name would would be created and then opend
 * @param  name The name of the file to open
 * @return      The index that the file was opened in
 */
int sfs_fopen(char *name)
{
	/* Check if there is space on the open FDT to add a new file */
	int emptyIndex = getEmptyOftIndex();
	int fileIndex, fatIndex, rdIndex;
	if(emptyIndex != NOT_FOUND)
	{
		fileIndex = getFileIndex(name);
		/* Check if the file is already open */
		if((fileIndex != NOT_FOUND) && (isClosed(name) == false))
		{
			printf("File already open. Value is \n");
			return ERROR;
		}
		/* If file is found and is not open, open it in append mode */
		if(fileIndex == NOT_FOUND)
		{
			/* If file not found create it and set the size to 0 */
			/* Check if the FAT is full */
			fatIndex = getEmptyFatIndex();
			if(fatIndex != NOT_FOUND)
			{
				fat.table[fatIndex].isEmpty = false;
				fat.table[fatIndex].next = END;
				fat.table[fatIndex].dbIndex =END;
				fat.index++;
				/* Check if there is space in the root directory to add another entry */
				if(rootDirectory.noFiles != FILE_NUMBER)
				{
					rdIndex = getEmptyRootDirecetoryIndex();
					/* Create a new file */
					rootDirectory.table[rdIndex].fatIndex = fatIndex;
					strcpy(rootDirectory.table[rdIndex].filename,name);
					rootDirectory.table[rdIndex].size = 0;
					rootDirectory.table[rdIndex].dateCreated = getTime();
					rootDirectory.table[rdIndex].dateModified = getTime();
					rootDirectory.table[rdIndex].readPtr = BEGINNING;
					rootDirectory.table[rdIndex].writePtr = BEGINNING;
					rootDirectory.table[rdIndex].isEmpty = false;
					rootDirectory.noFiles++;
					rootDirectory.index++;

					/* Open new file */
					oft.table[emptyIndex].fatIndex = rootDirectory.table[rdIndex].fatIndex;
					strcpy(oft.table[emptyIndex].filename, rootDirectory.table[rdIndex].filename);
					oft.table[emptyIndex].size = rootDirectory.table[rdIndex].size;
					oft.table[emptyIndex].dateCreated = rootDirectory.table[rdIndex].dateCreated;
					oft.table[emptyIndex].dateModified = rootDirectory.table[rdIndex].dateModified;
					oft.table[emptyIndex].readPtr = rootDirectory.table[rdIndex].readPtr;
					oft.table[emptyIndex].writePtr = rootDirectory.table[rdIndex].writePtr;
					oft.table[emptyIndex].isEmpty = rootDirectory.table[rdIndex].isEmpty;
					oft.index++;
					oft.noFiles++;
					writeToDisk();
					return emptyIndex;
				}
				else
				{
					printf("Not enough space in root directory to create file\n");
					/* Remove the fat entry */
					fat.table[fatIndex].isEmpty = true;
					fat.index--;
					return ERROR;
				}
			}
			else
			{
				printf("Not enough space on FAT to create a new file\n");
				return ERROR;
			}
		}
		else if(fileIndex >=0 && oft.table[emptyIndex].isEmpty && isClosed(name))
		{
			/* Open file in append mode */
			oft.table[emptyIndex].fatIndex = rootDirectory.table[fileIndex].fatIndex;
			strcpy(oft.table[emptyIndex].filename, rootDirectory.table[fileIndex].filename);
			oft.table[emptyIndex].size = rootDirectory.table[fileIndex].size;
			oft.table[emptyIndex].dateCreated = rootDirectory.table[fileIndex].dateCreated;
			oft.table[emptyIndex].dateModified = rootDirectory.table[fileIndex].dateModified;
			oft.table[emptyIndex].readPtr = BEGINNING;
			oft.table[emptyIndex].writePtr = BEGINNING;
			oft.table[emptyIndex].isEmpty = false;
			oft.noFiles++;
			oft.index++;
			return emptyIndex;
		}
		else
		{
			return ERROR;
		}
	}
	else
	{
		printf("Not enough space on Open File Descriptor Table to open %s\n", name);
		return ERROR;
	}
}

/**
 * Closes the file corresonding to a given file indentification number
 * @param  fileID The file indentification number
 * @return        Whether the operation was a sucess or an error was encountered
 */
int sfs_fclose(int fileID)
{
  if(oft.table[fileID].isEmpty)
  {
  	printf("File %i already closed\n", fileID);
  	return ERROR;
  }
  oft.noFiles--;
  oft.table[fileID].isEmpty = true;
  if(oft.noFiles == 0)
  {
    oft.index = BEGINNING;
  }
  return SUCCESS;
}

/**
 * Writes data from a given buffer with a given length to a file with
 * a given file indentification number
 * @param  fileID The file indetification number
 * @param  buf    The buffer containing the data that we want to write from
 * @param  length The size of the data in bytes
 * @return        The the size of the file
 */
int sfs_fwrite(int fileID, char *buf, int length){
    /* Do not try to write to the file if it is not oppened */
    if(oft.table[fileID].isEmpty == true){
        puts("File is not oppened\n");
        return ERROR;
    }
    //Check if there is free available space in the file
    int writePtr = oft.table[fileID].writePtr;
    int emptySpace = empty.noEmpty*BLOCK_SIZE;
    int residual = 0;
    int fileSize = oft.table[fileID].size;
    int writeTypes = 0;
    if(writePtr % BLOCK_SIZE != 0){
        residual = BLOCK_SIZE - (writePtr % BLOCK_SIZE);
    }
    if(residual != 0){
        writeTypes = 1;
    }
    /* Calculate the amount of free space required */
    int freeAllocatedSpace = (fileSize - writePtr)/BLOCK_SIZE;
    if( length > (emptySpace + residual + (freeAllocatedSpace*BLOCK_SIZE))){
        puts("Not enough available space\n");
        return ERROR;
    }
    int fatIndex;
    /* Calculate how much space needs to be allocated */
    if(length > (residual + freeAllocatedSpace*BLOCK_SIZE)){
        writeTypes += 100;
        int new_blocks_count = (length - residual -  freeAllocatedSpace*BLOCK_SIZE) / BLOCK_SIZE;
        if((length - residual -  freeAllocatedSpace) % BLOCK_SIZE != 0){
            new_blocks_count++;
        }
        fatIndex = oft.table[fileID].fatIndex;
        /* Get to the last FAT entry */
        while(fat.table[fatIndex].next != END){
            fatIndex = fat.table[fatIndex].next;
        }
        int fat_entry;
        /* Allocate the number of blocks needed */
        while(new_blocks_count > 0){
            fat_entry = getEmptyFatIndex();
            fat.index++;
            fat.table[fatIndex].next = fat_entry;
            fat.table[fatIndex].dbIndex = getEmptyBlockIndex();
            fat.table[fat_entry].next = END;
            fat.table[fat_entry].dbIndex = END;
            fatIndex = fat_entry;
            new_blocks_count--;
        }
    }
    int length_left =  length;
    char t_buf[BLOCK_SIZE];
    int i;

    /* Move pointer to the writing position */
    int write_location = writePtr / BLOCK_SIZE;
    if(writePtr % BLOCK_SIZE > 0){
        write_location++;
    }
    /* Take into account zero indexing */
    if(writePtr % BLOCK_SIZE > 0){
        write_location--;
    }
    fatIndex = oft.table[fileID].fatIndex;
    i = 0;
    while(i < write_location){
        fatIndex = fat.table[fatIndex].next;
        i++;
    }
    /* Start writing */
    if(writeTypes % 10 == 1){
        /* Overwrite current block */
        read_blocks(fat.table[fatIndex].dbIndex, 1, (void*) &t_buf);
        int tmp_writePtr = writePtr;
        tmp_writePtr %= BLOCK_SIZE;
        int length1;
        if(tmp_writePtr % BLOCK_SIZE !=0) length1 = BLOCK_SIZE - tmp_writePtr;
        else length1 = 0;
        if(length > length1){
            length_left -= length1;
        }else{
            length1 = length;
            length_left = 0;
        }
        while(length1 > 0){
            length1--;
            t_buf[tmp_writePtr] = *buf;
            tmp_writePtr++;
            *buf++;
        }
        write_blocks(fat.table[fatIndex].dbIndex, 1, &t_buf);
    }
    if(writeTypes > 10){
        /* Overwrite the block */
        if(writeTypes % 10 == 1) fatIndex = fat.table[fatIndex].next;
        while(length_left >= BLOCK_SIZE){
            memcpy(t_buf, buf, BLOCK_SIZE);
            write_blocks(fat.table[fatIndex].dbIndex, 1, &t_buf);
            i = 0;
            while(i < BLOCK_SIZE){
                *buf++;
                i++;
            }
            fatIndex = fat.table[fatIndex].next;
            length_left -= BLOCK_SIZE;
        }
        if(length_left > 0){
            /* Overwrite the currnet block */
            read_blocks(fat.table[fatIndex].dbIndex, 1, (void*) &t_buf);
            i = 0;
            while(length_left > 0){
                t_buf[i] = *buf;
                *buf++;
                i++;
                length_left--;
            }
            write_blocks(fat.table[fatIndex].dbIndex, 1, &t_buf);
        }
    }
    /* Update the oft table entry and the disk */
    oft.table[fileID].dateModified = getTime();
    oft.table[fileID].size += length;
    oft.table[fileID].writePtr += length;
    int fileIndex = getFileIndex(oft.table[fileID].filename);
    rootDirectory.table[fileIndex].dateModified = oft.table[fileID].dateModified;
    rootDirectory.table[fileIndex].size = oft.table[fileID].size;
    rootDirectory.table[fileIndex].writePtr = oft.table[fileID].writePtr;
    writeToDisk();
    return length;
}

/**
 * Reads data from the disk corresponding to a given file ID for and given length
 * and puts the data into the read data into the buffer.
 * @param  fileID The file identification number
 * @param  buf    The buffer that will store the data
 * @param  length The length of the file to be read
 * @return        The amount of data that was actually read
 */
int sfs_fread(int fileID, char *buf, int length)
{
	int totalLength= length;
	int spaceNeeded = length;
	int amountRead =0;
	/* Check if file is open so that it can be read */
	if(!(oft.table[fileID].isEmpty))
	{
		/* Get the actual length of the file */
		if(oft.table[fileID].size < totalLength)
		{
			spaceNeeded = oft.table[fileID].size;
		}
		/* Get the location of the read pointer */
		int readPtr = oft.table[fileID].readPtr;
		char buffer[BLOCK_SIZE];
		int fatIndex =oft.table[fileID].fatIndex;
		/* Get the location of the wanted data */
		int offset = readPtr/BLOCK_SIZE;
		while(offset > 0)
		{
			fatIndex = fat.table[fatIndex].next;
			offset--;
		}
		read_blocks(fat.table[fatIndex].dbIndex,1, (void *) &buffer);
		int readBytes = BLOCK_SIZE - (readPtr % BLOCK_SIZE);
		int readIndex = readPtr % BLOCK_SIZE;
		/* Make sure that you have not read more than the current file size */
		if(readBytes > spaceNeeded)
			readBytes = spaceNeeded;
		spaceNeeded -= readBytes;
		/* Read byte by byte */
		while(readBytes > 0)
		{
			*buf = buffer[readIndex];
			readBytes--;
			*buf++;
			readIndex++;
			amountRead++;
		}
		/* If actual length is not zero continue reading */
		while(spaceNeeded > 0)
		{
			fatIndex = fat.table[fatIndex].next; //get the next index
			read_blocks(fat.table[fatIndex].dbIndex,1, (void *) &buffer);
			readIndex = 0; //reset the read index
			/* Read byte by byte until the full file size has been read */
			while(spaceNeeded > 0 && readIndex < BLOCK_SIZE){
			    *buf = buffer[readIndex];
			    *buf++;
			    spaceNeeded--;
			    readIndex++;
			    amountRead++;
			}
		}
	}
	else
	{
		printf("File number %i is closed and therefore cannot be read\n", fileID);
		return ERROR;
	}
	/* Increment the oft and root read pointers */
	oft.table[fileID].readPtr += totalLength;
	rootDirectory.table[fileID].readPtr  = oft.table[fileID].readPtr;
	return amountRead;
}

/**
 * Seeks to the location of a file of the open file table with a given offeset to get
 * the location of of the data.
 * @param  fileID The file ID corresponding to a given file
 * @param  offset The offset within the block where the data is located
 * @return        If successful, the offeset, otherwise it returns NOT FOUND
 */
int sfs_fseek(int fileID, int offset)
{
  /* Seek to the location as long as it is with the file range */
  if((offset <= oft.table[fileID].size) && (offset >= 0))
  {
    if(!oft.table[fileID].isEmpty)
    {
      oft.table[fileID].readPtr = offset;
      oft.table[fileID].writePtr = oft.table[fileID].readPtr;
      return offset;
    }
    else
    {
      printf("Cannot seek to file ID: %i. File does not exist\n",fileID);
      return NOT_FOUND;
    }
  }
  else
  {
    printf("The location seeked is out of range\n");
    return ERROR;
  }

  return SUCCESS;
}

/**
 * Deletes a given file from the disk
 * @param  file The name of the file to be deleted
 * @return      SUCCESS if the file was remove and NOT_FOUND file the file was not found
 */
int sfs_remove(char *file)
{
  int fileIndex = getFileIndex(file);
  if(fileIndex != NOT_FOUND)
  {
  	/* Remove entry in root directory */
    rootDirectory.table[fileIndex].isEmpty = true;
    rootDirectory.noFiles--;
    /* Remove entry in FAT */
    int fatIndex = rootDirectory.table[fileIndex].fatIndex;
    fat.table[fatIndex].isEmpty = true;
    /* Update the corresponding empty table entry */
    if(fat.table[fatIndex].dbIndex != NOT_FOUND)
    {
      empty.table[fat.table[fatIndex].dbIndex] = true;
    }
    /* Remove all blocks and entries associlated with the file */
    while(fat.table[fatIndex].next != END)
    {
      fat.table[fatIndex].isEmpty = true;
      empty.table[fat.table[fatIndex].dbIndex] = true;
      fatIndex = fat.table[fatIndex].next; // get the next index
    }
    return SUCCESS;
  }
  else
  {
    return NOT_FOUND;
  }
}

/**
 * Gets the index of an empty block
 * @return The empty block index
 */
int getEmptyBlockIndex(){
  int i;
  if(empty.noEmpty> 0)
  {
    for(i = 2; i < BLOCK_NUMBER-1; i++)
    {
      if(empty.table[i] == true){
          empty.table[i] = false;
          empty.noEmpty--;
          return i;
      }
    }
  }
  return NOT_FOUND;
}

/**
 * Gets the index of an empty entry in the Root Directory
 * @return An empty root directory entry index
 */
int getEmptyRootDirecetoryIndex()
{
	int i;
	if(rootDirectory.index < FILE_NUMBER)
	{
		return rootDirectory.index;
	}
	else
	{
		for (i = 0; i < FILE_NUMBER; i++)
		{
			if (rootDirectory.table[i].isEmpty)
			{
				return i;
			}
		}
	}
	return NOT_FOUND;
}

/**
 * Gets the index of an empty entry in the file allocation table
 * @return An empty entry index in the FAT
 */
int getEmptyFatIndex()
{
	int i;
	if (fat.index < FAT_BLOCKS)
	{
		fat.table[fat.index].isEmpty = true;
		return fat.index;
	}
	else
	{
		for (i = 0; i < FAT_BLOCKS; i++)
		{
			if(fat.table[i].isEmpty)
				return i;
		}
	}
	return NOT_FOUND;
}

/**
 * Gets the index of an empty entry in the open file table
 * @return An empty index in the OFT
 */
int getEmptyOftIndex()
{
	int i;
	/* Check if there are unsed spaces in the oft */
	if (oft.index < FILE_NUMBER)
	{
		oft.table[oft.index].isEmpty = true;
		return oft.index;
	}
	else
	{
		for (i = 0; i < FILE_NUMBER; i++)
		{
			if (oft.table[i].isEmpty)
				return i;
		}
	}
	return NOT_FOUND;
}

/**
 * Gets the index corresponding to a given filename in the root directory
 * @param  filename The filename to be searched for
 * @return          The index in the root directory correspoinding to given filename
 */
int getFileIndex(char *filename)
{
	int i;
	for (i = 0; i < rootDirectory.index; i++)
	{
		if ((strcmp(filename,rootDirectory.table[i].filename) == 0) && (rootDirectory.table[i].isEmpty == false))
		{
			return i;
		}
	}
	return NOT_FOUND;
}

/**
 * Gets the index corrseponding to a given filename in the open file table
 * @param  filename The filename to be searched for
 * @return          The index in the OFT corresponding to the given fileanme
 */
int getOftFileIndex(char *filename)
{
	int i;
	for (i = 0; i < oft.index; i++)
	{
		if ((strcmp(filename,rootDirectory.table[i].filename) == 0) && (oft.table[i].isEmpty == false))
		{
			return i;
		}
	}
	return NOT_FOUND;
}

/**
 * Gets the current date and time
 * @return The current date and time
 */
time_t getTime()
{
	return time(NULL);
}

/**
 * Checks if the given filename is closed. Returns false if file is open or does not exist.
 * @param  filename The filename to check
 * @return          If the given file is closed
 */
bool isClosed(char *filename)
{
	int i;
	for (i = 0; i < FILE_NUMBER; i++)
	{
		if((strcmp(oft.table[i].filename,filename) == 0) && (oft.table[i].isEmpty))
		{
			return true;
		}
	}
	return false;
}

/**
 * Reads data from the disk by reading from the root directory, file allocation table and the empty table
 */
void readFromDisk()
{
	read_blocks(BEGINNING, 1, (void *) &rootDirectory);
	read_blocks(BEGINNING+1, 1, (void *) &fat);
	read_blocks(BLOCK_NUMBER-1, 1, (void *) &empty);
}

/**
 * Writes data to the disk by writing to the root directory, fle allocation table and the empty table
 */
void writeToDisk()
{
  write_blocks(BEGINNING, 1, (void *) &rootDirectory);
  write_blocks(BEGINNING+1, 1, (void *) &fat);
  write_blocks((BLOCK_NUMBER-1), 1, (void *) &empty);
}